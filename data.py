import numpy as np
import matplotlib.pyplot as plt
import pdb

class Random2DGaussian:

    def __init__(self, minx=0, maxx=10, miny=0, maxy=10):
        self._minx = minx
        self._maxx = maxx
        self._miny = miny
        self._maxy = maxy
        # expected values - mean
        self.mu = [(maxx - minx) * np.random.random_sample() + minx, (maxy - miny) * np.random.random_sample() + miny]
        # eigenvalues
        D = np.array([[(np.random.random_sample()*(maxx - minx)/5)**2, 0], [0, (np.random.random_sample()*(maxy - miny)/5)**2]])
        # rotation matrix
        sin_phi = np.random.random_sample()
        cos_phi = np.sqrt(1 - sin_phi**2)
        R = np.array([[cos_phi, sin_phi], [-sin_phi, cos_phi]])
        self.sigma = np.dot(np.dot(R.T, D), R)

    def get_sample(self, n=1):
        return np.random.multivariate_normal(self.mu, self.sigma, n)


def sample_gauss_2d(C, N):
    """
    C: int - number of distributions
    N: int - number of samples
    return: X - matrix of samples, Y - matrix (N x 1) of class indices
    """
    X = []
    Y = []
    for i in range(C):
        g = Random2DGaussian()
        X.extend(g.get_sample(N))
        Y.extend([i] * N)
    return np.array(X), np.array(Y)


def eval_perf_binary(Y, Y_):
    """
    Y: actual classes
    Y_: predicted classes
    return: accuracy, recall, precision
    """
    TP = 0
    FP = 0
    TN = 0
    FN = 0
    for y, y_ in zip(Y, Y_):
        if y == 1 and y_ == 1:
            TP += 1
        elif y == 1 and y_ == 0:
            FN += 1
        elif y == 0 and y_ == 1:
            FP += 1
        else:
            TN +=1
    accuracy = (TP + TN) / (TP + TN + FP + FN)
    recall = TP / (TP + FN)
    precision = TP / (TP + FP)
    return accuracy, recall, precision


def eval_AP(data):
    ap = 0
    N = len(data)
    for i in range(N):
        first_part = [0] * i
        second_part = [1] * (N - i)
        all_data = []
        all_data.extend(first_part)
        all_data.extend(second_part)
        (accuracy, recall, precision) = eval_perf_binary(data, all_data)
        ap += precision * data[i]
    return ap / sum(data)


def graph_data(X, Y_, Y):
    """
    X:  array-like - data for classification
    Y_: array-like - actual classes
    Y:  array-like - predicted classes
    """
    X_correct = []
    X_missed = []
    Y_correct = []
    Y_missed = []
    for i in range(len(Y)):
        color = 'gray' if Y_[i] == 0 else 'white'
        # correct classification
        if Y_[i] == Y[i]:
            X_correct.append(X[i])
            Y_correct.append(color)
        else:
            X_missed.append(X[i])
            Y_missed.append(color)
    X_correct = np.array(X_correct)
    X_missed = np.array(X_missed)
    # Y_correct = np.array(Y_correct)
    # Y_missed = np.array(Y_missed)
    if len(Y_correct):
        plt.scatter(X_correct[:,0], X_correct[:,1], c=Y_correct, marker='o')
    if len(Y_missed):
        plt.scatter(X_missed[:,0], X_missed[:,1], c=Y_missed, marker='s')
    plt.show()


def graph_surface(fun, rect, offset, width=640, height=480):
    """
    fun: function - decision function
    rect: tuple of list of len 2 - domain in which to show
    offset: float - decision threshold, e.g., 0.5
    width, height  - resolution of coordinates
    return: None
    """
    x = np.linspace(rect[0][0], rect[1][0])
    y = np.linspace(rect[0][1], rect[1][1])
    x_, y_ = np.meshgrid(x, y)
    X = np.dstack((x_, y_))
    Y = fun(X)
    # pdb.set_trace()
    plt.pcolormesh(x, y, Y, vmin=offset-0.5,vmax=offset+0.5)
    plt.contour(x, y, Y, levels=[offset])


def plot_problem(X, y, h=None, surfaces=True) :
    '''
    Plots a two-dimensional labeled dataset (X,y) and, if function h(x) is given,
    the decision boundaries (surfaces=False) or decision surfaces (surfaces=True)
    '''
    assert X.shape[1] == 2, "Dataset is not two-dimensional"
    if h!=None :
        # Create a mesh to plot in
        r = 0.02  # mesh resolution
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, r),
                             np.arange(y_min, y_max, r))
        XX=np.c_[xx.ravel(), yy.ravel()]
        try:
            Z_test = h(XX)
            if np.shape(Z_test) == () :
                # h returns a scalar when applied to a matrix; map explicitly
                Z = np.array(map(h,XX))
            else :
                Z = Z_test
        except ValueError:
            # can't apply to a matrix; map explicitly
            Z = np.array(map(h,XX))
        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        if surfaces :
            plt.contourf(xx, yy, Z, cmap=plt.cm.Pastel1)
        else :
            plt.contour(xx, yy, Z)
    # Plot the dataset
    plt.scatter(X[:,0],X[:,1],c=y, cmap=plt.cm.Paired,marker='o',s=50);


def sample_gmm_2d(K, C, N):
    """
    :param K: int - number of distributions from which to create return distributions
    :param C: int - number of return distributions
    :param N: int - number of samples per distribution
    :return X: - ndarray - data
            Y: - ndarray - class indices
    """
    # create bivariate distributions
    distributions = []
    classes = []
    X = []
    Y = []
    for i in range(K):
        g = Random2DGaussian()
        distributions.append(g)
        classes.append(random.randint(0, C-1))
    for i in range(K):
        # pick random distribution from which to form
        g = distributions[i]
        X.extend(g.get_sample(N))
        Y.extend([classes[i]] * N)
    return np.array(X), np.array(Y)


if __name__ == "__main__":
    # np.random.seed(100)
    G = Random2DGaussian()
    X = G.get_sample(100)
    plt.scatter(X[:,0], X[:,1])
    plt.show()
