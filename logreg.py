import matplotlib.pyplot as plt
import numpy as np
import pdb
import data

def softmax(X):
    exp_scores = np.exp(X)
    return exp_scores / np.sum(exp_scores, axis=1, keepdims=True)

def one_hot_encode(values):
    """
    :param values: arry like
    :return: ndarray len(values) x (max(values) + 1)
    """
    n_values = np.max(values) + 1
    return np.eye(n_values)[values]

def logreg_train(X, Y_, param_niter=1000, param_delta=0.005):
    """
    X:  array-like, N x n - data
    Y_: array-like, N x 1 - actual outputs
    return: W, b
            W: array-like, n x C
            b: array-like, 1 x C
    """
    # number of examples
    N = X.shape[0]
    # number of classes
    C = max(Y_) + 1
    # number of features
    n = X.shape[1]
    # initial values
    W = np.random.randn(n, C)
    b = np.zeros(C)
    # transform expected outputs in one-hot encoded features
    Y = one_hot_encode(Y_)
    for i in range(param_niter):
        # classification results
        scores = np.dot(X, W) + b
        # probabilities
        probs = softmax(scores)
        # loss
        log_probs = -1 * np.log(probs[range(N), Y_])
        loss = np.sum(log_probs) / N
        # diagnostic
        if i % 10 == 0:
            print("iteration: ", i, "loss: ", loss)
        # derivations
        dL_ds = probs - Y
        # gradients
        grad_W = np.dot(dL_ds.T, X).T
        grad_b = dL_ds.sum(axis=0)
        # apply changes
        W += -param_delta * grad_W
        b += -param_delta * grad_b
    return W,b

def logreg_classify(X, W, b):
    H = np.dot(X, W) + b
    return np.argmax(softmax(H), axis=1)

if __name__ == "__main__":
    X,Y_ = data.sample_gauss_2d(3, 50)
    plt.scatter(X[:,0], X[:,1], c=Y_)
    W,b = logreg_train(X, Y_)
    Y = logreg_classify(X, W, b)
    # pdb.set_trace()
    func = lambda x: logreg_classify(x, W, b)
    data.plot_problem(X, Y_, func)
    plt.show()
