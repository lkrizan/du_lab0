import data
import numpy as np
import pdb
import matplotlib.pyplot as plt

def sigm(x): return 1 / (1 + np.exp(-x))

def cross_entropy_loss(h_x, y):
    return -y * np.log(h_x) - (1 - y) * np.log(1 - h_x)

def binlogreg_train(X, Y_, param_niter=1000, param_delta=0.005):
    """
    X: data, array-like, N x D
    Y_: actual output, array-like, N x 1
    return: w, b - parameters for logistic regression
    """
    # N - number of examples
    N = X.shape[0]
    # n - number of dimensions
    n = X.shape[1]
    # intialize values
    w = np.random.randn(n)
    b = 0
    # actual training
    for i in range(param_niter):
        scores = np.dot(X, w) + b
        probs = sigm(scores)
        loss = cross_entropy_loss(probs, Y_).sum()
        # diagnostic print
        if i % 10 == 0:
            print("iteration: ", i, "loss: ", loss)
        # derivations of loss for every example
        dL_dscores = probs - Y_
        # gradients
        grad_w = np.dot(dL_dscores, X)
        grad_b = dL_dscores.sum()
        # update parameters
        w += -param_delta * grad_w
        b += -param_delta * grad_b
    return w,b

def binlogreg_classify(X, w, b):
    """
    X: input data, array-like, e.g., N x 2
    w, b: weights for logistic regression
    return: float - probability for class C1
    """
    return sigm(np.dot(X, w) + b)

if __name__ == "__main__":
    # get the training dataset
    X,Y_ = data.sample_gauss_2d(2, 100)

    # train the model
    w,b = binlogreg_train(X, Y_)

    # evaluate the model on the training dataset
    probs = binlogreg_classify(X, w,b)
    Y = [ 1 if a >= 0.5 else 0 for a in probs]

    # report performance
    accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
    AP = data.eval_AP(Y_[probs.argsort()])
    print (accuracy, recall, precision, AP)
    func = lambda x: binlogreg_classify(x, w, b)
    bbox=(np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(func, bbox, offset=0.5)
    data.graph_data(X, Y_, Y)
    plt.show()
